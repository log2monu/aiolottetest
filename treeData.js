export const treeData = [
  {
    type: 'Folder',
    child: [
      {
        type: 'File',
        child: null,
        name: 'File1',
      },
      {
        type: 'File',
        child: null,
        name: 'File2',
      },
      {
        type: 'Folder',
        child: [
          {
            type: 'File',
            child: null,
            name: 'File3',
          },
          {
            type: 'File',
            child: null,
            name: 'File4',
          },
        ],
        name: 'Folder3',
      },
      {
        type: 'Folder',
        child: [
            {
              type: 'File',
              child: null,
              name: 'File5',
            },
            {
              type: 'File',
              child: null,
              name: 'File6',
            },
          ],
        name: 'Folder4',
      },
    ],
    name: 'Folder1',
  },
  {
    type: 'Folder',
    child: [
      {
        type: 'File',
        child: null,
        name: 'File1',
      },
      {
        type: 'File',
        child: null,
        name: 'File2',
      },
      {
        type: 'Folder',
        child: [
          {
            type: 'File',
            child: null,
            name: 'File3',
          },
          {
            type: 'File',
            child: null,
            name: 'File4',
          },
        ],
        name: 'Folder5',
      },
      {
        type: 'Folder',
        child: [
            {
              type: 'File',
              child: null,
              name: 'File5',
            },
            {
              type: 'File',
              child: null,
              name: 'File6',
            },
          ],
        name: 'Folder6',
      },
    ],
    name: 'Folder2',
  },
];

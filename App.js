import React from 'react';
import {Text, View, TouchableOpacity, ImageBackground} from 'react-native';
import Icon1 from 'react-native-vector-icons/Feather';
import {treeData} from './treeData';

const App = () => {
  const [currentIndex, setCurrentIndex] = React.useState(null);

  const [currentCatIndex, setCurrentCatIndex] = React.useState(null);

  const [currentNestIndex, setCurrentNestIndex] = React.useState(null);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 20,
      }}>
      {treeData.map(({type, child, name}, index) => {
        return (
          <View key={index}>
            {type === 'Folder' ? (
              <TouchableOpacity
                onPress={() => {
                  setCurrentIndex(index === currentIndex ? null : index);
                }}
                activeOpacity={0.9}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingVertical: 4,
                    alignItems: 'center',
                  }}>
                  <Icon1
                    name={
                      index === currentIndex ? 'folder-minus' : 'folder-plus'
                    }
                    color={'#333'}
                    size={20}
                  />
                  <Text style={{marginHorizontal: 8, fontSize: 16}}>
                    {name}
                  </Text>
                </View>
                <Icon1
                  name={index === currentIndex ? 'chevron-up' : 'chevron-down'}
                  color={'#333'}
                  size={20}
                />
              </TouchableOpacity>
            ) : (
              <View style={{flexDirection: 'row'}}>
                <Icon1 name={'file'} color={'#333'} size={20} />
                <Text style={{marginHorizontal: 8, fontSize: 16}}>{name}</Text>
              </View>
            )}

            <View style={{marginHorizontal: 10}}>
              {index === currentIndex && (
                <View>
                  {child &&
                    child.map((subCategory, catIndex) => (
                      <View key={catIndex}>
                        {subCategory.type === 'Folder' ? (
                          <TouchableOpacity
                            onPress={() => {
                              setCurrentCatIndex(
                                catIndex === currentCatIndex ? null : catIndex,
                              );
                            }}
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                paddingVertical: 4,
                              }}>
                              <Icon1
                                name={
                                  catIndex === currentCatIndex
                                    ? 'folder-minus'
                                    : 'folder-plus'
                                }
                                color={'#333'}
                                size={20}
                              />
                              <Text style={{marginHorizontal: 8, fontSize: 16}}>
                                {subCategory.name}
                              </Text>
                            </View>
                            <Icon1
                              name={
                                catIndex === currentCatIndex
                                  ? 'chevron-up'
                                  : 'chevron-down'
                              }
                              color={'#333'}
                              size={20}
                            />
                          </TouchableOpacity>
                        ) : (
                          <View style={{flexDirection: 'row', paddingVertical: 3}}>
                            <Icon1 name={'file'} color={'#333'} size={20} />
                            <Text style={{marginHorizontal: 8, fontSize: 16}}>
                              {subCategory.name}
                            </Text>
                          </View>
                        )}

                        {catIndex === currentCatIndex && (
                          <View style={{marginHorizontal: 10}}>
                            {subCategory &&
                              subCategory.child &&
                              subCategory.child.map(
                                (subCategoryChild, nestIndex) => (
                                  <View key={nestIndex}>
                                    {subCategoryChild.type === 'Folder' ? (
                                      <TouchableOpacity
                                        onPress={() => {
                                          setCurrentNestIndex(
                                            nestIndex === currentNestIndex
                                              ? null
                                              : nestIndex,
                                          );
                                        }}>
                                        <View
                                          style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                          }}>
                                          <View
                                            style={{
                                              flexDirection: 'row',
                                              paddingVertical: 4,
                                            }}>
                                            <Icon1
                                              name={
                                                catIndex === currentCatIndex
                                                  ? 'folder-minus'
                                                  : 'folder-plus'
                                              }
                                              color={'#333'}
                                              size={20}
                                            />
                                            <Text
                                              style={{
                                                marginHorizontal: 8,
                                                fontSize: 16,
                                              }}>
                                              {subCategoryChild.name}
                                            </Text>
                                          </View>
                                          <Icon1
                                            name={
                                              catIndex === currentCatIndex
                                                ? 'chevron-up'
                                                : 'chevron-down'
                                            }
                                            color={'#333'}
                                            size={20}
                                          />
                                        </View>
                                      </TouchableOpacity>
                                    ) : (
                                      <View style={{flexDirection: 'row', paddingVertical: 3}}>
                                        <Icon1
                                          name={'file'}
                                          color={'#333'}
                                          size={20}
                                        />
                                        <Text
                                          style={{
                                            marginHorizontal: 8,
                                            fontSize: 16,
                                          }}>
                                          {subCategoryChild.name}
                                        </Text>
                                      </View>
                                    )}

                                    {nestIndex === currentNestIndex && (
                                      <View>
                                        {subCategoryChild &&
                                          subCategoryChild.child &&
                                          subCategoryChild.child.map(
                                            (nestChild, nestIndex1) => (
                                              <View
                                                style={{flexDirection: 'row'}}
                                                key={nestIndex1}>
                                                <Icon1
                                                  name={'file'}
                                                  color={'#333'}
                                                  size={20}
                                                />
                                                <Text
                                                  style={{
                                                    marginHorizontal: 8,
                                                    fontSize: 16,
                                                  }}>
                                                  {nestChild.name}
                                                </Text>
                                              </View>
                                            ),
                                          )}
                                      </View>
                                    )}
                                  </View>
                                ),
                              )}
                          </View>
                        )}
                      </View>
                    ))}
                </View>
              )}
            </View>
          </View>
        );
      })}
    </View>
  );
};

export default App;
